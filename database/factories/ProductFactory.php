<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    $image = $faker->randomElement([
        "https://img14.360buyimg.com/n7/jfs/t23023/190/556425254/138090/241c68a3/5b34a845N0be31c84.jpg",
        "https://img12.360buyimg.com/n7/jfs/t18778/102/2007550306/192841/d3490880/5ae2ca4dN434d0c6c.jpg",
        "https://img12.360buyimg.com/n7/jfs/t24286/83/1120948646/200088/acd4bb50/5b4ffc4fN04e20dbe.jpg",
        "https://img11.360buyimg.com/n7/jfs/t17098/342/1568169702/137246/cffb97d4/5ad011f4N786343d0.jpg",
        "https://img14.360buyimg.com/n7/jfs/t20644/262/2215747601/185299/a3172812/5b4c65a9N3a81ae6a.jpg",
        "https://img12.360buyimg.com/n7/jfs/t23599/358/1022950473/163770/dcf009f/5b4d8975N6018fbf7.jpg",
        "https://img11.360buyimg.com/n7/jfs/t20062/143/651188246/66378/d7bd1c43/5b04d4b9N36a179a4.jpg",
        "https://img10.360buyimg.com/n7/jfs/t23941/327/533635206/326360/185ff554/5b3340b5Nd8b54cb8.jpg",
        "https://img12.360buyimg.com/n7/jfs/t17038/35/1876555903/64417/888c041c/5adb098aNd46b2f9b.jpg",
        "https://img14.360buyimg.com/n7/jfs/t21838/150/415570274/237795/7e4fbe84/5b0cf730Nc444f1be.jpg",
    ]);
    return [
        'title'        => $faker->word,
        'description'  => $faker->sentence,
        'image'        => $image,
        'on_sale'      => true,
        'rating'       => $faker->numberBetween(0, 5),
        'sold_count'   => 0,
        'review_count' => 0,
        'price'        => 0,
    ];
});
