# Larashop

#### 项目介绍
基于laravel5.5的商城，学习使用

#### 软件架构
软件架构说明
1.注册与登录：php artisan make:auth ;  注册测试：php artisan migrate
2.邮箱验证：①php artisan make:migration users_add_email_verified --table=users
②调整模型类;创建中间件：make:middleware
使用的是qq邮箱，只需开启服务，在.env配置就ok.注册完成过后系统会发送激活邮件，通过 Laravel 的事件系统来完成这个功能：make:listener
3.优雅的处理异常:用户行为和系统内部
4.收货地址列表
5.新增地址
6.编辑和修改地址
7.优化交互:ajax提交，用sweetalert
8.检查权限：只允许地址的拥有者来修改和删除地址。通过授权策略类（Policy）来实现权限控制
9.初始化管理后台，使用 laravel-admin
10.管理后台-用户列表
11.商品模型
12.管理后台 - 商品列表
13.管理后台 - 新增、编辑商品:重点增加一对多的
14.用户界面-商品列表：重点图片的显示，生成数据,php artisan db:seed --class=ProductsSeeder
15.商品的筛选和排序，重点：传值到搜索框和分页的查询参数，以及监听下拉框的change事件来触发表单自动提交
16.商品详情页,重点：输出 SKU 的按钮的变化，相应的库存和价钱的变化
17.收藏商品，重点：收藏商品本质上是用户和商品的多对多关联，不需要创建新模型，只需增加一个中间表，收藏商品和取消收藏的功能
18.商品列表
19.添加商品到购物车：选择数据库来保存购物车的数据
20.购物车页面：页面展示以及删除和全选问题
21.订单模型
22.购物车下单页面
23.关闭未支付页面:用 Laravel 提供的延迟任务（Delayed Job）功能,队列需要用到redis，开启redis，Laravel 生成的 .env 文件里把队列的驱动设置成了 sync（同步），在同步模式下延迟任务会被立即执行，所以需要先把队列的驱动改成 redis，引用composer require predis/predis,记得要开启redis:redis-server
24.用户订单列表
25.用户订单详情页,限权控制：通过授权策略类（Policy）来实现，通过 make:policy 命令创建一个授权策略类
26.封装业务代码
27.引入支付库->配置参数-》容器->测试（tinker）:composer require yansongda/pay(yansongda/pay 这个库封装了支付宝和微信支付的接口，通过这个库我们就不需要去关注不同支付平台的接口差异，使用相同的方法、参数来完成支付功能，节省开发时间)
 app('alipay')
=> Yansongda\Pay\Gateways\Alipay {#2945}
>>> app('wechat_pay')
=> Yansongda\Pay\Gateways\Wechat {#2948}
28.支付宝支付->获取支付宝沙箱参数（https://openhome.alipay.com/platform/appDaily.htm?tab=info）
29.订单的支付宝支付->写支付方法->路由->添加支付入口->写支付回调(前端回调和服务器回调)
30.微信支付-》获取商家号
31.微信生成二维码（composer require endroid/qr-code）
32.完善支付后逻辑->商品增加销量（创建监听器，关联事件和监听器）->发送邮件通知（创建通知类，创建监听器，关联事件和监听器）
33.后台订单管理模块
#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)