<?php
/**
 * Created by PhpStorm.
 * User: HCDX
 * Date: 2018/7/20
 * Time: 11:36
 * by Hedy<714195347@qq.com>
 */
namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Throwable;

class InvalidRequestException extends Exception
{
    //用户行为异常处理
    public function __construct(string $message = "", int $code = 400)
    {
        parent::__construct($message, $code);
    }
    public function rend(Request $request)
    {
        if($request->expectsJson()){
            return response()->json(['msg'=>$this->message],$this->code);
        }
        return view('pages.error',['msg'=>$this->message]);
    }
}
