<?php
/**
 * Created by PhpStorm.
 * User: HCDX
 * Date: 2018/7/19
 * Time: 18:04
 * by Hedy<714195347@qq.com>
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function root()
    {
        return view('pages.root');
    }

    /**
     * 邮箱验证页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
   public function emailVerifyNotice(Request $request)
   {
       return view('pages.email_verify_notice');
   }
}