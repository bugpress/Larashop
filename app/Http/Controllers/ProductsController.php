<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/7/22
 * Time: 16:46
 * by Hedy<714195347@qq.com>
 */
namespace App\Http\Controllers;

use App\Exceptions\InvalidRequestException;
use Illuminate\Http\Request;
use App\Models\Product;
class ProductsController extends Controller
{
    public function index(Request $request)
    {
        /**
         * 创建查询构造器
         * where('on_sale', true) 筛选出 on_sale 字段为 true 的记录，这样未上架的商品就不会被展示出
         */
        //$products = Product::query()->where('on_sale',true)->paginate(16);
        $builder = Product::query()->where('on_sale',true);
        //判断是否有提交search参数，如果有就赋值给$search变量，search参数是用来模糊搜索商品
        if($search = $request->input('search','')){
            $like = '%'.$search.'%';
            //模糊搜索商品的标题、商品详情、SKU标题、SKU描述
            $builder->where(function ($query) use ($like){
                $query->where('title','like',$like)
                    ->orWhere('description','like',$like)
                    ->orWhereHas('skus',function ($query) use ($like){
                        $query->where('title','like',$like)
                            ->orWhere('description','like',$like);
                    });
            });
        }

        /**
         * 是否有提交order参数，如果有就赋值给$order变量
         * order参数用来控制商品的排序规则
         *
         */
        if($order = $request->input('order','')){
            //是否以_asc 或者 _desc结尾
            if(preg_match('/^(.+)_(asc|desc)$/',$order,$m)){
                //如果字符串的开头是这3个字符串之一，说明是一个合法的排序值
                if(in_array($m[1],['price','sold_count','rating'])){
                    //根据传入的排序值来构造排序参数
                    $builder->orderBy($m[1],$m[2]);
                }
            }
        }
        $products = $builder->paginate(16);
        return view('products.index',[
            'products'=>$products,
            'filters' =>[
                'search' => $search,
                'order'  => $order,
            ],
        ]);
    }

    public function show(Product $product,Request $request)
    {
        //判断商品是否已经上架，如果没有上架就抛出异常
        if(!$product->on_sale){
            throw new InvalidRequestException('商品未上架');
        }//要在页面添加 取消收藏 按钮及其功能，对于已经收藏了当前商品的用户，我们不展示 加入收藏 按钮，而展示 取消收藏 按钮，因此需要在控制器中把收藏状态注入到模板中
        $favored = false;
        //用户未登录时返回的是null，已经登录返回的是对应的用户对象
        if($user = $request->user()){
            // 从当前用户已收藏的商品中搜索 id 为当前商品 id 的商品
            // boolval() 函数用于把值转为布尔值
            $favored = boolval($user->favoriteProducts()->find($product->id));
        }
        return view('products.show',[
            'product'  => $product,
            'favored'  => $favored,
        ]);
    }

    /**
     * 收藏商品
     * @param Product $product
     * @param Request $request
     * @return array
     * 这里还可以写成 attach($product->id)
     */
    public function favor(Product $product, Request $request)
    {
        $user = $request->user();
        if ($user->favoriteProducts()->find($product->id)) {
            return [];
        }

        $user->favoriteProducts()->attach($product);

        return [];
    }

    /**
     * 取消收藏
     * @param Product $product
     * @param Request $request
     * @return array
     * detach() 方法用于取消多对多的关联，接受的参数个数与 attach() 方法一致
     */
    public function disfavor(Product $product,Request $request)
    {
        $user = $request->user();
        $user->favoriteProducts()->detach($product);
        return [];
    }

    public function favorites(Request $request)
    {
        $products = $request->user()->favoriteProducts()->paginate(16);
        //dd($products);
        return view('products.favorites',['products'=>$products]);
    }
}
