<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/7/25/025
 * Time: 22:24
 * By Hedy<714195347@qq.com>
 */

namespace App\Http\Controllers;


use App\Exceptions\InternalException;
use App\Exceptions\InvalidRequestException;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\Request;
use App\Jobs\CloseOrder;
use App\Models\Order;
use App\Models\ProductSku;
use App\Models\Product;
use App\Models\UserAddress;
use App\Services\CartService;
use App\Services\OrderService;
use Carbon\Carbon;

class OrdersController extends Controller
{
    /**
     * 订单列表页展示
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //使用with方法预加载，避免N+1问题,items是product和produsku的中间表
        $orders = Order::query()
            ->with(['items.product','items.productSku'])
            ->where('user_id',$request->user()->id)
            ->orderBy('created_at','desc')
            ->paginate();
        //dd($orders);
        return view('orders.index',['orders'=>$orders]);
    }

    /**
     * 创建订单
     * @param OrderRequest $request
     * @return mixed
     */
    public function store(OrderRequest $request,OrderService $orderService)
    {
        $user = $request->user();
        $address = UserAddress::find($request->input('address_id'));
        return  $orderService->store($user,$address,$request->input('remark'),$request->input('items'));
        //开启一个数据库的事务
        // 别忘了把 $cartService 加入 use 中
//        $order = \DB::transaction(function () use ($user,$request,$cartService){
//            $address = UserAddress::find($request->input('address_id'));
//            //更新此地址的最后使用时间
//            $address->update(['last_used_at'=>Carbon::now()]);
//            //创建一个订单
//            $order = new Order([
//                'address'    =>[
//                    //将地址放在订单中
//                    'address' => $address->full_address,
//                    'zip'     => $address->zip,
//                    'contact_name' => $address->contact_name,
//                    'contact_phone' => $address->contact_phone
//                ],
//                //订单备注
//                'remark' => $request->input('remark'),
//                'total_amount' => 0,
//            ]);
//            //订单关联到当前用户
//            $order->user()->associate($user);
//            //写入数据库
//            $order->save();
//            $totalAmount = 0;
//            $items = $request->input('items');
//            //遍历用户提交的sku
//            foreach ($items as $data){
//                $sku = ProductSku::find($data['sku_id']);
//                //创建一个cartItem 并直接与当前订单关联
//                //$order->items()->make() 方法可以新建一个关联关系的对象=$item = new OrderItem(); $item->order()->associate($order)
//                $item = $order->items()->make([
//                    'amount' => $data['amount'],
//                    'price'  => $sku->price,
//                ]);
//                $item->product()->associate($sku->product_id);
//                $item->productSku()->associate($sku);
//                $item->save();
//                $totalAmount +=$sku->price* $data['amount'];
//                if($sku->decreaseStock($data['amount'])<=0){
//                    throw new InternalException('库存量不足');
//                }
//            }
//
//            //更新订单金额
//            $order->update(['total_amount'=>$totalAmount]);
//
//            //将下单的商品从购物车中移除
//            //使用collect() 辅助函数快速取得所有 SKU ID，然后将本次订单中的商品 SKU 从购物车中删除。
//            $skuIds = collect($request->input('items'))->pluck('sku_id')-all();
////            $user->cartItems()->whereIn('product_sku_id',$skuIds)->delete();
//            $cartService->remove($skuIds);
//            //在创建订单之后，触发关闭订单的操作,从配置文件中读取设置的关闭时间
//            $this->dispatch(new CloseOrder($order,config('app.order_ttl')));
//            return $order;
//        });
//
//        //dd($order);
//
//        return $order;
    }


    /**
     * 详情页
     * @param Order $order
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Order $order,Request $request)
    {
        //这里的 load() 方法与上一章节介绍的 with() 预加载方法有些类似，称为 延迟预加载，不同点在于 load() 是在已经查询出来的模型上调用，而 with() 则是在 ORM 查询构造器上调用。
        //校验权限
        $this->authorize('own',$order);
        return view('orders.show',[
            'order'=>$order->load(['items.productSku','items.product'])
        ]);
    }

    public function received(Order $order,Request $request)
    {
        //效验权限
        $this->authorize('own',$order);
        // 判断订单的发货状态是否为已发货
        if ($order->ship_status !== Order::SHIP_STATUS_DELIVERED) {
            throw new InvalidRequestException('发货状态不正确');
        }
        // 更新发货状态为已收到
        $order->update(['ship_status' => Order::SHIP_STATUS_RECEIVED]);
        //返回原页面
//        return redirect()->back();
        //们把确认收货的操作从表单提交改成了 AJAX 请求，因此控制器中的返回值需要修改一下
        return $order;
    }
}