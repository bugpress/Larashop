<?php
/**
 * Created by PhpStorm.
 * User: HCDX
 * Date: 2018/7/19
 * Time: 11:17
 * by Hedy<714195347@qq.com>
 */
namespace App\Http\Middleware;

use Closure;

class CheckIfEmailVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //当email_verified不是true时，就重定向
        if(!$request->user()->email_verified){
            if($request->expectsJson()){
                return response()->json(['msg'=>'请先验证邮箱'],400);
            }
            return redirect(route('email_verify_notice'));
        }
        //执行下一个中间件
        return $next($request);
    }
}
