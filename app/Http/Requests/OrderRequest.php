<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/7/25/025
 * Time: 22:27
 * By Hedy<714195347@qq.com>
 */

namespace App\Http\Requests;


use App\Models\ProductSku;
use Illuminate\Validation\Rule;

class OrderRequest extends Request
{
    //对数据进行校验
    public function rules()
    {
        return [
            // 判断用户提交的地址 ID 是否存在于数据库并且属于当前用户
            // 后面这个条件非常重要，否则恶意用户可以用不同的地址 ID 不断提交订单来遍历出平台所有用户的收货地址
            'address_id' => ['required',Rule::exists('user_addresses','id')->where('user_id',$this->user()->id)],
            'items' => ['required','array'],
            'items.*.sku_id' =>[
                //检查items数组下面的每一个子数组的sku_id
                'required',
                function($attribute,$value,$fail){
                    if(!$sku = ProductSku::find($value)){
                        $fail('该商品不存在');
                        return;
                    }
                    if(!$sku->product->on_sale){
                        $fail('该商品未上架');
                        return ;
                    }
                    if(!$sku->stock === 0){
                        $fail('该商品已售完');
                        return;
                    }

                    //检测库存量
                    //获取当前索引
                    preg_match('/items\.(\d+)\.sku_id/',$attribute,$m);
                    $index = $m[1];
                    //根据索引找到用户提交的购买数量
                    //采用正则的方式将这个 0 提取出来，$this->input('items')[0]['amount'] 就是用户想购买的数量
                    $amount = $this->input('items')[$index]['amount'];
                    //进行判断
                    if($amount > 0 && $amount > $sku->stock){
                        $fail('该商品库存不够(；′⌒`)');
                        return;
                    }
                }
            ],
            'items.*.amount' => ['required','integer','min:1'],
        ];
    }
}