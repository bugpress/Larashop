<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Monolog\Logger;
use Yansongda\Pay\Pay;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // 往服务容器中注入一个名为 alipay 的单例对象
        //app()->environment() 获取当前运行的环境，线上环境会返回 production。对于支付宝，如果项目运行环境不是线上环境，则启用开发模式，并且将日志级别设置为 DEBUG。由于微信支付没有开发模式，所以仅仅将日志级别设置为 DEBUG。
        $this->app->singleton('alipay',function (){
            $config = config('pay.alipay');
            //回调
            //回调地址必须是完整的带有域名的 URL，不可以是相对路径。使用 route() 函数生成的 URL 默认就是带有域名的完整地址。
            $config['notify_url'] ='http://requestbin.fullcontact.com/14khytx1';// route('payment.alipay.notify');
            $config['return_url'] = route('payment.alipay.return');
            //判断当前项目运行环境是否是线上环境
            if(app()->environment()!=='production'){
                $config['mode']   = 'dev';
                $config['log']['level'] = Logger::DEBUG;
            }else{
                $config['log']['level'] = Logger::WARNING;
            }
            //调用yansongda\pay来创建一个支付宝支付对象
            return Pay::alipay($config);
        });
        $this->app->singleton('wechat_pay',function (){
            $config = config('pay.wechat');
            //配置回调地址
            $config['notify_url'] = 'http://requestbin.fullcontact.com/[替换成你自己的url]';
            //判断当前项目运行环境是否是线上环境
            if(app()->environment()!=='production'){
                $config['log']['level'] = Logger::DEBUG;
            }else{
                $config['log']['level'] = Logger::WARNING;
            }
            //调用yansongda\pay来创建一个微信支付对象
            return Pay::wechat($config);
        });
    }
}
