<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['amount','price','rating','review','reviewed_at'];
    protected $dates = ['reviewed_at'];
    public $timestamps = false;

    //order_items 则保存商品 SKU ID、数量以及与 orders 表的关联
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productSku()
    {
        return $this->belongsTo(ProductSku::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
