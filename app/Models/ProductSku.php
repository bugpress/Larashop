<?php

namespace App\Models;

use App\Exceptions\InternalException;
use Illuminate\Database\Eloquent\Model;

class ProductSku extends Model
{
    protected $fillable = ['title', 'description', 'price', 'stock'];

    /**
     * 与product的关系是多对一
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * 减少库存量的变化
     * @param $amount
     * @return int
     * @throws InternalException
     */
    public function decreaseStock($amount)
    {
        if($amount<0){
            throw new InternalException('减库存不可少于0');
        }
        //$this->newQuery() 方法来获取数据库的查询构造器，ORM 查询构造器的写操作只会返回 true 或者 false 代表 SQL 是否执行成功，而数据库查询构造器的写操作则会返回影响的行数。
        return $this->newQuery()->where('id',$this->id)->where('stock','>=',$amount)->decrement('stock',$amount);
    }

    /**
     * 增加库存量
     * @param $amount
     * @throws InternalException
     */
    public function addStock($amount)
    {
        if($amount<0){
            throw new InternalException('加库存量不可小于0');
        }

        //需通过 increment() 方法来保证操作的原子性
        $this->increment('stock',$amount);
    }
}
