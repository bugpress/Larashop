<?php
/**
 * Created by PhpStorm.
 * User: HCDX
 * Date: 2018/7/20
 * Time: 13:58
 * by Hedy<714195347@qq.com>
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable=[
        'province',
        'city',
        'district',
        'address',
        'zip',
        'contact_name',
        'contact_phone',
        'last_used_at',
    ];
    //返回一个时间的对象
    protected $dates = ['last_used_at'];

    /**
     * 跟User模型的关联，一对多，一个User可以有多个UserAddress
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 创建访问器，可通过$address->full_address来获取完整地址
     * @return string
     */
    public function getFullAddressAttribute()
    {
        return "{$this->province}{$this->city}{$this->district}{$this->address}";
    }

}
