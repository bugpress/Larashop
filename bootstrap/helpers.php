<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/7/19/019
 * Time: 22:02
 * by Hedy<714195347@qq.com>
 */
function test_helper(){
    return 'OK';
}
//route_class() 是我们自定义的辅助方法，我们还需要在 helpers.php 文件中添加此方法：
function route_class()
{
    return str_replace('.','-',Route::currentRouteName());
}